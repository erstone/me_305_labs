## @File       Lab1.py
# @author     Eric Stone
# @copyright  This file is licensed under the creative commons


def fib (idx):
    '''
    @brief This method calculates a Fibonacci number corresponding to a specified index.
    @details This is a Fibonacci function that uses a bottom up approach.
    It takes a user input that takes user variable idx and......
    The file can be found: https://bitbucket.org/erstone/me_305_labs/src/master/Lab%201/Lab1.py
    @param idx An integer specifying the index of the desired Fibonacci number.
    '''
    idx = input('Please enter a positive whole number or type "quit" to exit: ')
    if idx == 'quit':
        pass
    else:
        try:
            idx = int(idx)
            if idx < 0:
                print('Must enter a POSITIVE whole number')
                fib(0)
            else:
                if idx < 2:     
                    value = idx
                    print ('Calculating Fibonacci number at '
                           'index n = {:}.'.format(idx))
                    print ('Fibonacci number = ', value)
                    pass
                else:
                    value = 0
                    num = idx
                    i = 0
                    j = 1
                    while num > 0:
                        value = i + j
                        i = j
                        j = value
                        num = num - 1
                        pass
                    print ('Calculating Fibonacci number at '
                           'index n = {:}.'.format(idx))
                    print ('Fibonacci number = ', value)
                fib(0)
        except ValueError:
            print('Only integers or "quit" are accepted')
            fib(0)
        
if __name__ == '__main__':
    fib(0)

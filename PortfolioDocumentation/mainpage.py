## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_Lab1 Lab1 Info
#  My Fibonacci code (Lab1) can be found at: https://bitbucket.org/erstone/me_305_labs/src/master/Lab%201/Lab1.py
#  @section sec_Lab2 Lab2 Info
#  My Lab2 code (Lab2) can be found at:
#  @section sec_Lab3 Lab3 Info
#  My Lab3 code (Lab3) can be found at:
#  @section sec_Lab4 Lab4 Info
#  My Lab4 code (Lab4) can be found at:
#  @section sec_Lab5 Lab5 Info
#  My Lab5 code (Lab5) can be found at:
#  @section sec_Lab6 Lab6 Info
#  My Lab6 code (Lab6) can be found at:
#  @author Eric Stone
#
#  @copyright Creative Commons
#
#  @date September 17, 2020
#